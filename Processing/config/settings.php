<?php

return [
    'data_path' => $_ENV['NY_TAXIS_DATASET_PATH'] ?? realpath(__DIR__.'/../var/data'),
];
