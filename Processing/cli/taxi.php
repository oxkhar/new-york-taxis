#!/usr/bin/env php
<?php

require __DIR__."/../vendor/autoload.php";

$config = require __DIR__."/../config/settings.php";

$app = new \Silly\Edition\PhpDi\Application("New York taxis", "beta");

$app->command(
    'dataset:green:clean [-i|--in=] [-o|--out=] [-e|--err=]',
    \Oxkhar\NyTaxis\Command\CleanGreenDataset::class
)->descriptions(
    "Prepare and clean data about green taxis into a new dataset",
    [
        "--in"  => "Dataset file about green taxis",
        "--out" => "Dataset file to write clean data",
        "--err" => "Dataset file with wrong records",
    ]
)->defaults(
    [
        "in"  => $config['data_path'].'/green.csv',
        "out" => $config['data_path'].'/green_clean.csv',
        "err" => $config['data_path'].'/green_error.csv',
    ]
);

$app->command(
    'dataset:yellow:clean [-i|--in=] [-o|--out=] [-e|--err=]',
    \Oxkhar\NyTaxis\Command\CleanYellowDataset::class
)->descriptions(
    "Prepare and clean data about yellow taxis in a new dataset",
    [
        "--in"  => "Dataset file about green taxis",
        "--out" => "Dataset file to write clean data",
        "--err" => "Dataset file with wrong records",
    ]
)->defaults(
    [
        "in"  => $config['data_path'].'/yellow.csv',
        "out" => $config['data_path'].'/yellow_clean.csv',
        "err" => $config['data_path'].'/yellow_error.csv',
    ]
);

$app->command(
    'dataset:ny-taxis:stack [-g|--green=] [-y|--yellow=] [-o|--out=] [-e|--err=]',
    \Oxkhar\NyTaxis\Command\NyTaxisStackDataset::class
)->descriptions(
    "Append clean datasets for each type of taxis into a unique dataset",
    [
        "--yellow" => "Clean dataset file about yellow taxis",
        "--green"  => "Clean dataset file about green taxis",
        "--out"    => "Dataset file to stack data",
        "--err"    => "Dataset file with wrong records",
    ]
)->defaults(
    [
        "yellow" => $config['data_path'].'/yellow_clean.csv',
        "green"  => $config['data_path'].'/green_clean.csv',
        "out"    => $config['data_path'].'/ny-taxis-stack.csv',
        "err"    => $config['data_path'].'/ny-taxis_error.csv',
    ]
);

$app->run();
