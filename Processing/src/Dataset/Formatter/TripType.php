<?php

namespace Oxkhar\NyTaxis\Dataset\Formatter;

use Oxkhar\NyTaxis\Dataset\Field;

class TripType
{
    use FormatterTrait;

    public const FIELD = Field::TRIP_TYPE;

    public const ENUMS = [
        '1' => 'Cogido en la calle',
        '2' => 'Enviado desde la parada de taxis',
    ];

    public function __invoke(array $data): array
    {
        $data[self::FIELD] = (string)intval($data[self::FIELD]);

        return $this->convertTypes($data);
    }
}
