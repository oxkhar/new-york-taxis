<?php

namespace Oxkhar\NyTaxis\Dataset\Formatter;

use Oxkhar\NyTaxis\Dataset\Field;

class VendorId
{
    use FormatterTrait;

    public const FIELD = Field::VENDOR;

    public const ENUMS = [
        "1" => "Creative Mobile Technologiess, LLC",
        "2" => "VeriFone Inc.",
    ];

    public function __invoke(array $data): array
    {
        return $this->convertTypes($data);
    }
}
