<?php

namespace Oxkhar\NyTaxis\Dataset\Formatter;

use Oxkhar\NyTaxis\Dataset\Field;

class TripTime
{
    public function __invoke(array $data): array
    {
        $pickupDate = new \DateTimeImmutable($data[Field::PICKUP_DATETIME]);
        $dropOffData = new \DateTimeImmutable($data[Field::DROPOFF_DATETIME]);

        $data[Field::TRIP_TIME] = $this->seconds($dropOffData->diff($pickupDate, false));

        return $data;
    }

    private function seconds(\DateInterval $interval): int
    {
        return 0 +
            24 * 60 * 60 * (int)$interval->d +
            60 * 60 * (int)$interval->h +
            60 * (int)$interval->i +
            (int)$interval->s;
    }
}
