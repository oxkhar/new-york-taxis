<?php

namespace Oxkhar\NyTaxis\Dataset\Formatter;

class RecordFormat
{
    private $header;

    public function __construct(array $header)
    {
        $this->header = array_combine($header, $header);
    }

    public function __invoke(array $record): array
    {
        return array_map(
            function ($field) use ($record) {
                return $record[$field] ?? null;
            },
            $this->header
        );
    }
}
