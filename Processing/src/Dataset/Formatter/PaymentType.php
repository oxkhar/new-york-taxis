<?php

namespace Oxkhar\NyTaxis\Dataset\Formatter;

use Oxkhar\NyTaxis\Dataset\Field;

class PaymentType
{
    use FormatterTrait;

    public const FIELD = Field::PAYMENT_TYPE;

    public const ENUMS = [
        '1' => 'Tarjeta de crédito',
        '2' => 'Efectivo',
        '3' => 'Sin cargo',
        '4' => 'Disputa',
        '5' => 'Desconocido',
        '6' => 'Viaje anulado',
    ];

    public function __invoke(array $data): array
    {
        return $this->convertTypes($data);
    }
}
