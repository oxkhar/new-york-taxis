<?php

namespace Oxkhar\NyTaxis\Dataset\Formatter;

use Oxkhar\NyTaxis\Dataset\Field;

class RateCode
{
    use FormatterTrait;

    public const FIELD = Field::RATE_CODE;

    public const ENUMS = [
        '1' => 'Tasa estándar',
        '2' => 'JFK',
        '3' => 'Newark',
        '4' => 'Nassau o Westchester',
        '5' => 'Tarifa negociada',
        '6' => 'Viaje de grupo',
    ];

    public function __invoke(array $data): array
    {
        return $this->convertTypes($data);
    }
}
