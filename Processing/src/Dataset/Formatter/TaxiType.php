<?php

namespace Oxkhar\NyTaxis\Dataset\Formatter;

use Oxkhar\NyTaxis\Dataset\Field;

class TaxiType
{
    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function __invoke(array $data): array
    {
        $data[Field::TAXI_TYPE] = $this->type;

        return $data;
    }
}
