<?php

namespace Oxkhar\NyTaxis\Dataset\Formatter;

trait FormatterTrait
{
    private function convertTypes(array $data): array
    {
        assert(defined("self::ENUMS"), new \ErrorException("Enum of types not defined"));
        assert(defined("self::FIELD"), new \ErrorException("Field name for types not defined"));

        $data[self::FIELD] = self::ENUMS[$data[self::FIELD]] ?? $this->error($data[self::FIELD]);

        return $data;
    }

    private function error($data): string
    {
        return 'ERROR('.$data.')';
    }
}
