<?php

namespace Oxkhar\NyTaxis\Dataset\Formatter;

use Oxkhar\NyTaxis\Dataset\Field;

class Datetime
{
    public function __invoke(array $data): array
    {
        $dateDropoff = new \DateTimeImmutable($data[Field::DROPOFF_DATETIME]);
        $data[Field::DROPOFF_MONTH] = $dateDropoff->format('n');
        $data[Field::DROPOFF_DAY] = $dateDropoff->format('j');
        $data[Field::DROPOFF_HOUR] = $dateDropoff->format('H');

        $datePickup = new \DateTimeImmutable($data[Field::PICKUP_DATETIME]);
        $data[Field::PICKUP_MONTH] = $datePickup->format('n');
        $data[Field::PICKUP_DAY] = $datePickup->format('j');
        $data[Field::PICKUP_HOUR] = $datePickup->format('H');

        return $data;
    }
}
