<?php

namespace Oxkhar\NyTaxis\Dataset;

use League\Csv\Reader;

class GreenTaxis
{
    /**
     * Order and name of fields in a record of dataset
     */
    public const HEADER = [
        Field::VENDOR,
        Field::PICKUP_DATETIME,
        Field::DROPOFF_DATETIME,
        Field::STORE_N_FWD_FLAG,
        Field::RATE_CODE,
        Field::PICKUP_LONGITUDE,
        Field::PICKUP_LATITUDE,
        Field::DROPOFF_LONGITUDE,
        Field::DROPOFF_LATITUDE,
        Field::PASSENGER_COUNT,
        Field::TRIP_DISTANCE,
        Field::FARE_AMOUNT,
        Field::EXTRA,
        Field::MTA_TAX,
        Field::TIP_AMOUNT,
        Field::TOLLS_AMOUNT,
        Field::IMPROVEMENT_SURCHARGE,
        Field::TOTAL_AMOUNT,
        Field::PAYMENT_TYPE,
        Field::TRIP_TYPE,
    ];

    public function records(string $pathDataset): \Iterator
    {
        return Reader::createFromPath($pathDataset, 'r')
            ->setDelimiter(';')
            ->setHeaderOffset(0)
            ->getRecords(self::HEADER);
    }
}
