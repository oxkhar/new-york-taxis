<?php

namespace Oxkhar\NyTaxis\Dataset\Validator;

class FieldTypes
{
    public const NAME = 'field type must belong to a list of values';

    private $enums;

    public function __construct(array $enums)
    {
        $this->enums = $enums;
    }

    public function __invoke(array $data): bool
    {
        return array_reduce(
            $this->enums,
            function ($isValid, $formatter) use ($data) {
                return $isValid && in_array($data[$formatter::FIELD], $formatter::ENUMS);
            },
            true
        );
    }
}
