<?php

namespace Oxkhar\NyTaxis\Dataset\Validator;

use League\Geotools\Coordinate\Coordinate;
use League\Geotools\Polygon\Polygon;
use Oxkhar\NyTaxis\Dataset\Field;

class PositionNearNewYork
{
    /**
     * Define a polygon area where geopositions have to be contained
     */
    public const POLYGON_AREA = [
        [41.47850, -75.00350],
        [41.47850, -71.42196],
        [40.32532, -71.42196],
        [40.32532, -75.00350],
    ];

    public const NAME = "latitude and longitude must be near NY";

    /**
     * @var \League\Geotools\Polygon\Polygon
     */
    private $polygon;

    public function __construct()
    {
        $this->polygon = new Polygon(self::POLYGON_AREA);
        $this->polygon->setPrecision(5); // set the comparision precision
    }

    public function __invoke(array $data): bool
    {
        return
            $this->isPointInArea($data[Field::PICKUP_LATITUDE], $data[Field::PICKUP_LONGITUDE]) &&
            $this->isPointInArea($data[Field::DROPOFF_LATITUDE], $data[Field::DROPOFF_LONGITUDE]);
    }

    private function isPointInArea($lat, $long)
    {
        return $this->polygon->pointInPolygon(new Coordinate([floatval($lat), floatval($long)]));
    }
}
