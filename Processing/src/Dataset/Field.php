<?php

namespace Oxkhar\NyTaxis\Dataset;

interface Field
{
    const VENDOR = 'vendor_id';
    const PICKUP_DATETIME = 'pickup_datetime';
    const PICKUP_LONGITUDE = 'pickup_longitude';
    const PICKUP_LATITUDE = 'pickup_latitude';
    const DROPOFF_DATETIME = 'dropoff_datetime';
    const DROPOFF_LONGITUDE = 'dropoff_longitude';
    const DROPOFF_LATITUDE = 'dropoff_latitude';
    const TRIP_TYPE = 'trip_type';
    const TRIP_DISTANCE = 'trip_distance';
    const RATE_CODE = 'rate_code';
    const PASSENGER_COUNT = 'passenger_count';
    const PAYMENT_TYPE = 'payment_type';
    const TOTAL_AMOUNT = 'total_amount';
    const TIP_AMOUNT = 'tip_amount';
    const TOLLS_AMOUNT = 'tolls_amount';
    const FARE_AMOUNT = 'fare_amount';
    const EXTRA = 'extra';
    const MTA_TAX = 'mta_tax';
    const IMPROVEMENT_SURCHARGE = 'improvement_surcharge';
    const STORE_N_FWD_FLAG = 'store_and_fwd_flag';
    const TAXI_TYPE = 'taxi_type';
    const TRIP_TIME = 'trip_time';
    const DROPOFF_MONTH = 'dropoff_month';
    const DROPOFF_DAY = 'dropoff_day';
    const DROPOFF_HOUR = 'dropoff_hour';
    const PICKUP_MONTH = 'pickup_month';
    const PICKUP_DAY = 'pickup_day';
    const PICKUP_HOUR = 'pickup_hour';
}
