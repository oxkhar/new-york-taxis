<?php

namespace Oxkhar\NyTaxis\Dataset;

use League\Csv\Reader;

class YellowTaxis
{
    /**
     * Order and name of fields in a dataset record
     */
    public const HEADER = [
        Field::VENDOR,
        Field::PICKUP_DATETIME,
        Field::DROPOFF_DATETIME,
        Field::PASSENGER_COUNT,
        Field::TRIP_DISTANCE,
        Field::PICKUP_LONGITUDE,
        Field::PICKUP_LATITUDE,
        Field::RATE_CODE,
        Field::STORE_N_FWD_FLAG,
        Field::DROPOFF_LONGITUDE,
        Field::DROPOFF_LATITUDE,
        Field::PAYMENT_TYPE,
        Field::FARE_AMOUNT,
        Field::EXTRA,
        Field::MTA_TAX,
        Field::TIP_AMOUNT,
        Field::TOLLS_AMOUNT,
        Field::IMPROVEMENT_SURCHARGE,
        Field::TOTAL_AMOUNT,
    ];

    public function records(string $pathDataset): \Iterator
    {
        return Reader::createFromPath($pathDataset, 'r')
            ->setDelimiter(';')
            ->setHeaderOffset(0)
            ->getRecords(self::HEADER);
    }
}
