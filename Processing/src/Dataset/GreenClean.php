<?php

namespace Oxkhar\NyTaxis\Dataset;

use League\Csv\Reader;
use League\Csv\Writer;

class GreenClean
{
    const HEADER = [
        Field::VENDOR,
        Field::PICKUP_DATETIME,
        Field::DROPOFF_DATETIME,
        Field::PICKUP_MONTH,
        Field::PICKUP_DAY,
        Field::PICKUP_HOUR,
        Field::DROPOFF_MONTH,
        Field::DROPOFF_DAY,
        Field::DROPOFF_HOUR,
        Field::PASSENGER_COUNT,
        Field::TRIP_DISTANCE,
        Field::PICKUP_LATITUDE,
        Field::PICKUP_LONGITUDE,
        Field::DROPOFF_LATITUDE,
        Field::DROPOFF_LONGITUDE,
        Field::PAYMENT_TYPE,
        Field::FARE_AMOUNT,
        Field::EXTRA,
        Field::MTA_TAX,
        Field::TIP_AMOUNT,
        Field::TOLLS_AMOUNT,
        Field::IMPROVEMENT_SURCHARGE,
        Field::TOTAL_AMOUNT,
        Field::RATE_CODE,
        Field::TRIP_TIME,
        Field::TRIP_TYPE,
        Field::TAXI_TYPE,
        Field::STORE_N_FWD_FLAG,
    ];

    public function writer(string $pathDataset): Writer
    {
        $writer = Writer::createFromPath($pathDataset, 'w')
            ->setDelimiter(';');

        $writer->insertOne(self::HEADER);

        $this->addFormatters($writer);
        $this->addValidators($writer);

        return $writer;
    }

    protected function addFormatters(Writer $writer): void
    {
        $writer
            ->addFormatter(new Formatter\TaxiType('green'))
            ->addFormatter(new Formatter\VendorId())
            ->addFormatter(new Formatter\Datetime())
            ->addFormatter(new Formatter\PaymentType())
            ->addFormatter(new Formatter\RateCode())
            ->addFormatter(new Formatter\TripTime())
            ->addFormatter(new Formatter\TripType())
            ->addFormatter(new Formatter\RecordFormat(self::HEADER));
    }

    protected function addValidators(Writer $writer): void
    {
        $writer
            ->addValidator(
                new Validator\FieldTypes(
                    [
                        Formatter\PaymentType::class,
                        Formatter\TripType::class,
                        Formatter\RateCode::class,
                        Formatter\VendorId::class,
                    ]
                ),
                Validator\FieldTypes::NAME
            )
            ->addValidator(
                new Validator\PositionNearNewYork(),
                Validator\PositionNearNewYork::NAME
            );
    }

    public function error(string $pathDataset): Writer
    {
        $writer = Writer::createFromPath($pathDataset, 'w')
            ->setDelimiter(';');

        $header = self::HEADER;
        $header[] = 'error';

        $writer->insertOne($header);

        return $writer;
    }

    public function records(string $pathDataset): \Iterator
    {
        return Reader::createFromPath($pathDataset, 'r')
            ->setDelimiter(';')
            ->setHeaderOffset(0)
            ->getRecords(self::HEADER);
    }
}
