<?php

namespace Oxkhar\NyTaxis\Dataset;

use League\Csv\Writer;

class NyTaxisStack
{
    /**
     * Order and name of fields in a record of dataset
     */
    const HEADER = [
        Field::VENDOR,
        Field::TAXI_TYPE,
        Field::PICKUP_DATETIME,
        Field::PICKUP_MONTH,
        Field::PICKUP_DAY,
        Field::PICKUP_HOUR,
        Field::DROPOFF_DATETIME,
        Field::DROPOFF_MONTH,
        Field::DROPOFF_DAY,
        Field::DROPOFF_HOUR,
        Field::PICKUP_LATITUDE,
        Field::PICKUP_LONGITUDE,
        Field::DROPOFF_LATITUDE,
        Field::DROPOFF_LONGITUDE,
        Field::TRIP_DISTANCE,
        Field::TRIP_TIME,
        Field::PASSENGER_COUNT,
        Field::RATE_CODE,
        Field::PAYMENT_TYPE,
        Field::TOTAL_AMOUNT,
        Field::FARE_AMOUNT,
        Field::TIP_AMOUNT,
        Field::TOLLS_AMOUNT,
        Field::EXTRA,
        Field::MTA_TAX,
        Field::IMPROVEMENT_SURCHARGE,
        Field::STORE_N_FWD_FLAG,
        Field::TRIP_TYPE,
    ];

    public function writer(string $pathDataset): Writer
    {
        $writer = Writer::createFromPath($pathDataset, 'w')
            ->setDelimiter(';');

        $writer->insertOne(self::HEADER);

        $writer->addFormatter(new Formatter\RecordFormat(self::HEADER));

        return $writer;
    }

    public function error(string $pathDataset): Writer
    {
        $writer = Writer::createFromPath($pathDataset, 'w')
            ->setDelimiter(';');

        $header = self::HEADER;
        $header[] = 'error';

        $writer->insertOne($header);

        return $writer;
    }
}
