<?php

namespace Oxkhar\NyTaxis\Command;

use League\Csv\CannotInsertRecord;
use League\Csv\Writer;
use Oxkhar\NyTaxis\Dataset\GreenClean;
use Oxkhar\NyTaxis\Dataset\NyTaxisStack;
use Oxkhar\NyTaxis\Dataset\YellowClean;
use Symfony\Component\Console\Output\OutputInterface;

class NyTaxisStackDataset
{
    /**
     * @var \Oxkhar\NyTaxis\Dataset\GreenClean
     */
    private $yellowDataset;

    /**
     * @var \Oxkhar\NyTaxis\Dataset\NyTaxisStack
     */
    private $outputDataset;

    /**
     * @var \Oxkhar\NyTaxis\Dataset\GreenClean
     */
    private $greenDataset;

    public function __construct(
        YellowClean $yellowDataset,
        GreenClean $greenDataset,
        NyTaxisStack $outputDataset
    ) {
        $this->yellowDataset = $yellowDataset;
        $this->greenDataset = $greenDataset;
        $this->outputDataset = $outputDataset;
    }

    public function __invoke(
        string $green,
        string $yellow,
        string $out,
        string $err,
        OutputInterface $output
    ) {
        $output->writeln("<info>Stack clean datasets into a unique dataset</info>");

        $writer = $this->outputDataset->writer($out);
        $errWriter = $this->outputDataset->error($err);

        $datasets = [
            $yellow => $this->yellowDataset,
            $green  => $this->greenDataset,
        ];
        foreach ($datasets as $in => $dataset) {
            $output->write("<info>Reading $in...</info>");

            $this->stackDataset($dataset->records($in), $writer, $errWriter, $output);
        }
    }

    protected function stackDataset(
        \Iterator $records,
        Writer $writer,
        Writer $errWriter,
        OutputInterface $output
    ): void {

        $bytes = $errBytes = 0;
        foreach ($records as $record) {
            try {
                $bytes += $writer->insertOne($record);
            } catch (CannotInsertRecord $exc) {
                $errData = $exc->getRecord();
                $errData[] = $exc->getName();
                $errBytes += $errWriter->insertOne($errData);

                $output->write('<error>.</error>');
            }
        }

        $output->writeln(
            "\n<info>Write ".
            number_format($bytes, 0, ',', '.').
            " bytes in ".$writer->getPathname()."</info>"
        );
        if (!empty($errBytes)) {
            $output->writeln(
                "<error>Error in ".
                number_format($errBytes, 0, ',', '.').
                " bytes, see details in ".$errWriter->getPathname()."</error>"
            );
        }
    }
}
