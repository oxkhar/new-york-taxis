<?php

namespace Oxkhar\NyTaxis\Command;

use League\Csv\CannotInsertRecord;
use Oxkhar\NyTaxis\Dataset\YellowClean;
use Oxkhar\NyTaxis\Dataset\YellowTaxis;
use Symfony\Component\Console\Output\OutputInterface;

class CleanYellowDataset
{
    /**
     * @var \Oxkhar\NyTaxis\Dataset\YellowTaxis
     */
    private $inputDataset;

    /**
     * @var \Oxkhar\NyTaxis\Dataset\YellowClean
     */
    private $outputDataset;

    public function __construct(
        YellowTaxis $inputDataset,
        YellowClean $outputDataset
    ) {
        $this->inputDataset = $inputDataset;
        $this->outputDataset = $outputDataset;
    }

    public function __invoke(
        string $in,
        string $out,
        string $err,
        OutputInterface $output
    ) {
        $output->writeln("<info>Clean dataset for yellow taxis</info>");

        $records = $this->inputDataset->records($in);
        $writer = $this->outputDataset->writer($out);
        $errWriter = $this->outputDataset->error($err);

        $output->write("<info>Reading $in...</info>");
        $bytes = $errBytes = 0;
        foreach ($records as $record) {
            try {
                $bytes += $writer->insertOne($record);
            } catch (CannotInsertRecord $exc) {
                $errData = $exc->getRecord();
                $errData[] = $exc->getName();
                $errBytes += $errWriter->insertOne($errData);

                $output->write('<error>.</error>');
            }
        }

        $output->writeln(
            "\n<info>Write ".
            number_format($bytes, 0, ',', '.').
            " bytes in $out</info>"
        );
        if (!empty($errBytes)) {
            $output->writeln(
                "<error>Error in ".
                number_format($errBytes, 0, ',', '.').
                " bytes, see details in $err</error>"
            );
        }
    }
}
