#!/bin/bash

#alias php="winpty docker run --rm -ti -v /$(eval 'echo $(pwd)'):/app -w //app php:7.3 -f"
#alias composer="winpty docker run --rm -ti -v /$(eval 'echo $(pwd)'):/app -w //app composer"

[ -e .env ] && . .env

DOCKER_PHP_IMG=${DOCKER_REGISTRY}${APP_IMAGE}:cli
DOCKER_COMPOSE_FILE=docker/dev/docker-compose.yaml

DOCKER_RUN_OPTIONS="
    --rm
    -u $(id -u):$(id -g)
    -e PHP_CLI=php
    -e COMPOSER_HOME=${COMPOSER_HOME:-/home/composer}
    -v composer:${COMPOSER_HOME:-/home/composer}
    -v /etc/group:/etc/group:ro
    -v /etc/passwd:/etc/passwd:ro
    -w /app
"

docker-run () {
    local tty=
    tty -s && tty="--tty --interactive"
    docker run ${tty} ${DOCKER_RUN_OPTIONS} -v $(pwd):/app "$@"
}

php-run() {
    docker-run ${DOCKER_PHP_IMG} "$@"
}

composer () {
    docker-run composer "$@"
}

docker-compose () {
    bin/docker-compose -f ${DOCKER_COMPOSE_FILE} --project-directory $(pwd) "$@"
}

docker-compose-run () {
    docker-compose run ${DOCKER_RUN_OPTIONS} -v $(pwd):/app "$@"
}

php-cli() {
    docker-compose-run cli "$@"
}

php-dbg() {
    docker-compose-run cli \
        -dzend_extension=xdebug.so  \
        -dxdebug.coverage_enable=1  \
        -dxdebug.remote_enable=1    \
        -dxdebug.remote_port=${XDEBUG_REMOTE_PORT:-172.17.0.1} \
        -dxdebug.remote_host=${XDEBUG_REMOTE_HOST:-9000} \
        "$@"
}

phpunit() {
    php-cli bin/phpunit "$@"
}

phpunit-coverage() {
    php-dbg bin/phpunit --coverage-text "$@"
}

behat() {
    php-cli bin/behat "$@"
}

phpstan() {
    php-cli bin/phpstan "$@"
}

aliases-uninstall() {
    unset DOCKER_PHP_IMG DOCKER_COMPOSE_FILE DOCKER_RUN_OPTIONS
    unset -f docker-compose docker-compose-run docker-run \
        php-run php-cli php-dbg composer phpunit phpunit-coverage \
        behat phpstan aliases-uninstall
}
