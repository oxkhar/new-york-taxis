import DashboardLayout from "../layout/DashboardLayout";

import Overview from '../pages/Overview'
import CartoMap from "../pages/CartoMap";
import CartoActivity from "../pages/CartoActivity";

const routes = [
    {
        path: '/',
        component: DashboardLayout,
        redirect: '/dashboard',
        children: [
            {
                path: 'dashboard',
                name: 'Overview',
                component: Overview
            },
            {
                path: 'activity',
                name: 'CartoMap',
                component: CartoMap
            },
            {
                path: 'zone',
                name: 'CartoActivity',
                component: CartoActivity
            }
        ]
    }
]

export default routes
