import Vue from 'vue'
import Router from 'vue-router'

import routes from './routes/routes'

Vue.use(Router)

export default new Router({
    routes: routes,
    mode: 'history',
    base: process.env.BASE_URL,
    linkActiveClass: 'nav-item active',
    scrollBehavior: (to) => {
        if (to.hash) {
            return {selector: to.hash}
        } else {
            return {x: 0, y: 0}
        }
    }
})
